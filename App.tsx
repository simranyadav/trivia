/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {type PropsWithChildren} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { Question } from './src/component/Question';



const App = () => {
  const isDarkMode = useColorScheme() === 'dark';


  return (
    <SafeAreaView >
      <StatusBar 
      />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.backgroundStyle}>
        <View><Text style={styles.welcomeText}>Welcome to {'\n'}Trivia Quiz Game</Text></View>
        <View>
         <Question/>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create<any>({
  backgroundStyle:{
    backgroundColor:'white'
  },
  welcomeText:{
    fontSize:24,
    color:'black',
    fontWeight:'bold',
    justifyContent:'center',
    textAlign:'center',
    marginTop:50
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
