
import React, {useEffect, useState, type PropsWithChildren} from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    TextInput,
    Text,
    TouchableOpacity,
    useColorScheme,
    View,
    Dimensions,
    Alert
  } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';  
import { RequestApi } from '../requests/api';
const deviceWidth=Dimensions.get('window').width

export const Question = () => {
  const [questionList,setQuestion]= useState<any>()
  const [index,setIndex] = useState<number>(0)
  const [answer, setAnswer] = useState<string>('')
  const [error,setError] = useState<string>('')
  const [errorColor,setErrorColor] = useState<string>('')
  const [marks,setMarks] = useState<number>(0)
  const [youScore,setYouscrore] = useState<string>('')
  

  useEffect(()=>{
    RequestApi((data:any)=>{
        setQuestion(data.results)
        setIndex(0)
      })
  },[])  

  const matchAnswer=()=>{
    if(index===9)
    {setYouscrore('marks')}
    setError('')
    if(questionList[index].correct_answer.toLowerCase()===answer.toLowerCase().trim()){
        setMarks(marks+1)
        setError('Correct Answer')
        setErrorColor('green')
    }else{
        setError('Invalid Answer')
        setErrorColor('red')
    }
  }

  const isDarkMode = useColorScheme() === 'dark';
  console.log('question list==',questionList)
  return (
    <View style={styles.sectionContainer}>
      <Text style={{textAlign:'right',fontWeight:'bold'}}>Score : {marks}/10</Text>
      {youScore!='marks' && <View> 
      {questionList && <Text style={styles.sectionTitle}>
       Q.{index+1} {questionList[index].question}
      </Text>}
      <TextInput
        style={styles.input}
        onChangeText={setAnswer}
        value={answer}
        placeholder="Enter Answer"
      />
      {error!='' && <Text style={[styles.sectionDescription,{color:errorColor}]}>{error}</Text>}
      {questionList && error==='Invalid Answer' && <Text style={[styles.sectionDescription,{color:'green'}]}>{questionList[index].correct_answer}</Text>}
      {/* <View style={{flexDirection:'row',justifyContent:'space-between'}}> */}
       {error==='' && <TouchableOpacity style={styles.buttonstyle} onPress={()=>matchAnswer()} disabled={answer!=""?false:true}>
            <Text style={{color:'white',fontSize:20,textAlign:'center',fontWeight:'bold'}}>Submit</Text>
        </TouchableOpacity>}
        { error!='' && <TouchableOpacity onPress={()=>{setAnswer(''),setError(''),setYouscrore(index===9?'marks':''),index<9?setIndex(index+1) :setYouscrore('marks')}} style={styles.buttonstyle}>
            <Text style={{color:'white',fontSize:20,textAlign:'center',fontWeight:'bold'}}>{"Next>>"}</Text>
        </TouchableOpacity>}

        </View> }
        {youScore!='' && <View style={styles.yourScoreStyle}>
        <Text style={{fontSize:20,fontWeight:'bold'}}>You score {marks} out of 10</Text>
        </View>
        }

      {/* </View> */}
       
    </View>
  );
};

const styles = StyleSheet.create({
    sectionContainer: {
      marginTop: 32,
      paddingHorizontal: 24

    },
    sectionTitle: {
      fontSize: 16,
      fontWeight: '400',
      color:'bleck',
      marginTop:20
    },
    sectionDescription: {
      marginTop: 10,
      fontSize: 17,
      fontWeight: '400',
      color:'red',
      justifyContent:'center',
      textAlign:'center'
    },
    highlight: {
      fontWeight: '700',
    },
    input: {
        height: 40,
        marginTop: 12,
        borderWidth: 1,
        padding: 10,
        borderRadius:50
      },
    buttonstyle:{
        height:40,
        width:'100%',
        backgroundColor:'black',
        borderRadius:50,
        marginTop:40,
        justifyContent:'center'},
    yourScoreStyle:{
        alignItems:'center',
        marginTop:10,
        textAlign:'center',
        fontWeight:'bold',
        fontSize:20,
        borderWidth:1,
        height:60,
        justifyContent:'center',
        borderRadius:10
        }      
  });